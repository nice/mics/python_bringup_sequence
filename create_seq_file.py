"""Python script to create EPICS sequencer text file."""

import argparse
import re
from pathlib import Path

address_maps_label = {
    "M": "RING-REGS-MST",
    "C": "CTL-REGS-MST",
    "T": "TIME-REGS-MST",
    "E": "ENG-REGS-MST",
    "S": "RING-REGS-SLV",
    "U": "USR-REGS-SLV",
}


def create_seq_file(
    infile: str,
    output: str,
    space_prefix: str,
) -> None:
    """Translate R5560 regs txt file to registers json file."""
    epics_txt_file = open(output, "w")

    with open(infile) as f:
        lines = f.readlines()
        for line in lines:
            if line[0] == "#" in line:
                continue

            # remove double space
            line_complete: str = line.replace("  ", " ")
            # remove number of lines:
            line_complete = line_complete.split(" ", 1)[1]

            # get command name:
            cmd_name = line_complete.split(" ")[0]

            # get registers type
            regs_type = line_complete.split(" ")[1]

            if cmd_name != "MSG":
                # Remove regs type
                line_complete = line_complete.replace(f" {regs_type} ", " ")

            # if cmd_name is equal PLOCK, the reg will be the 5th param,
            # and it is necessary remove the LCKD reg
            # and the 7 (polling attempts):
            # E.g: the line PLOCK LCKD 0x00000010 0xffffffef RS04 7,
            # should be like this:
            # PLOCK %s-%s:RING-REGS-MST-RS-04 0x00000010 0xffffffef

            ic2_cmds = ["I2CW1", "I2CW2", "DELAY", "I2CR1_s"]

            if cmd_name == "MSG":
                epics_txt_file.write(line_complete)
                continue

            elif cmd_name == "PLOCK":
                # get the rx snapshot register name
                reg = line_complete.split(" ")[4]

                line_plock = line_complete.replace("LCKD", reg)
                line_complete = ""
                for idx, param in enumerate(line_plock.split(" ", 4)):
                    if idx < 4:
                        line_complete += param + " "
                line_complete = line_complete[:-1]
                line_complete += "\n"

                _reg = convert_regs_to_epicsSFX(reg, cmd_name, regs_type, space_prefix)
                # Replace the register name from PV SFX.
                line_complete = line_complete.replace(reg, _reg)

            elif cmd_name == "SOFTWARE_TOF_CALC":
                # NOTE: SW_TOF_CALC always use the register TMOF

                line_complete = line_complete.replace(
                    "SOFTWARE_TOF_CALC", "SW_TOF_CALC"
                )
                line_complete = line_complete[: line_complete.find("TMOF")] + "\n"
                settnst = line_complete.split()[1]
                settgot = line_complete.split()[2]

                _reg = convert_regs_to_epicsSFX(
                    settnst, cmd_name, regs_type, space_prefix
                )
                line_complete = line_complete.replace(settnst, _reg)

                _reg = convert_regs_to_epicsSFX(
                    settgot, cmd_name, regs_type, space_prefix
                )
                line_complete = line_complete.replace(settgot, _reg)
                line_complete = line_complete.replace(" \n", "\n")

            elif cmd_name in ic2_cmds:
                line_complete = convert_hex_to_decimal(line_complete)

            else:
                reg = line_complete.split(" ")[1]
                new_reg_sfx = convert_regs_to_epicsSFX(
                    reg, cmd_name, regs_type, space_prefix
                )
                # Replace the register name from PV SFX.
                line_complete = line_complete.replace(reg, new_reg_sfx)
                if line_complete[-1] != "\n":
                    line_complete += "\n"

            # for the slaver register, we use only the get and set commands
            # if regs_file == "S":
            #    # Remove regs type
            #    line_complete = line_complete.replace(" S ", " ")
            #    reg = (line_complete.split(" ")[1])
            #    new_reg_sfx = convert_slave_regs_to_epicsSFX(reg, cmd_name)
            #    # Replace the register name from PV SFX.
            #    line_complete = line_complete.replace(reg, new_reg_sfx)
            #    if line_complete[-1] != '\n':
            #        line_complete += '\n'

            line_complete = convert_hex_to_decimal(line_complete)
            epics_txt_file.write(line_complete)

    epics_txt_file.close()


def convert_hex_to_decimal(line_string):
    """Convert hex to decimal number."""
    list_hex_values = re.findall(r"0x[0-9A-F]+", line_string, re.I)
    for hex_value in list_hex_values:
        int_value = int(hex_value, 16)
        line_string = line_string.replace(hex_value, str(int_value))

    return line_string


def convert_regs_to_epicsSFX(reg_name, cmd_name, addres_map_macro, space_prefix=None):
    """Convert the register name to EPICS PV name."""
    slave_strings = {"S", "U"}

    if addres_map_macro not in slave_strings:
        for idx, character in enumerate(reg_name):
            if character.isdigit():
                reg_name = reg_name[:idx] + "-" + reg_name[idx:]
                break

    reg_name = reg_name.replace("\n", "")
    reg_name = reg_name.replace("_", "-")

    if addres_map_macro in slave_strings:
        node = int(reg_name.rsplit("-", 1)[-1])
        ring = int(reg_name.rsplit("-", 2)[-2])
        reg_name = f"{reg_name.rsplit('-', 2)[0]}-R{ring:02d}-N{node:02d}"

    if "mac--" in reg_name:
        lw_up_word = "LW" if reg_name[-3] == "0" else "UW"
        reg_name = reg_name[:-4] + reg_name[-2:] + "-" + lw_up_word

    if "ip" and "addr" in reg_name:
        reg_name += "-RAW"

    set_pv_cmds = ["SET", "MSK_SET"]
    if cmd_name in set_pv_cmds:
        reg_name += "-SP"
    else:
        reg_name += "-RBV"

    if not space_prefix:
        space_prefix = address_maps_label[addres_map_macro]

    return "%s-%s:" + space_prefix + "-" + reg_name.upper()


def main():
    """Python script to create EPICS sequencer text file."""
    parser = argparse.ArgumentParser(
        description="Script that translate the r5560 registers text file into json regs file"
    )
    parser.add_argument(
        "-i", "--infile", type=Path, required=True, help="ring command file"
    )
    parser.add_argument(
        "-o", "--output", type=Path, required=True, help="output epics txt file"
    )
    parser.add_argument("-s", "--space-prefix", type=str, help="space prefix")
    args = parser.parse_args()
    create_seq_file(**vars(args))


if __name__ == "__main__":
    main()
