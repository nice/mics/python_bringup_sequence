"""Python script to create json file from vendor header file.
"""

import argparse
import logging
import sys
from pathlib import Path
from typing import Dict, List, TextIO


def text_out(text: str, fout: TextIO):
    fout.write(text + '\n')
    

def parse_line(line: str) -> List[str]:
    # Remove initial part of string and remove _OFFSET string
    new_line = line[8:].replace("_OFFSET", "")
    
    # Replace LSB to LO
    new_line = new_line.replace("_LSB", "_LO")

    # Replace MSB to HI
    new_line = new_line.replace("_MSB", "_HI")

    # Remove _REG if the register ends with this suffix
    if new_line.split()[0][-4:] == "_REG":
        new_line = new_line.replace("_REG", "")
    return new_line.split()


def translate_vendor_header_file(
    infile: str,
    output: str,
    space_label: str,
    address_offset: str,
    space_full_name: str
) -> None:
    """Translate vendor header file to registers json file."""

    fout = open(output + ".json", "w")

    text_out("{", fout)
    text_out('    "project name"     : "Detector Group Readout Master",', fout)
    text_out(f'    "space full name"  : "{space_full_name}",', fout)
    text_out(f'    "space label"      : "{space_label}",', fout)
    text_out('    "axi_library"      : "dgro_fea",', fout)
    text_out('    "addr bus width"   : "32",', fout)
    text_out(f'    "address offset"   : "{address_offset}",', fout)
    text_out('    "parameter map"    : [', fout)

    with open(infile) as f:
        lines = f.readlines()
        get_access_mode = False
        offset_line = ""
        complete_line = ""
        for line in lines:
            if 'OFFSET' in line:
                offset_line = parse_line(line)
                get_access_mode = False
            if 'ACCESS' in line:
                if not get_access_mode:
                    access_mode = line.strip()[-2:]
                    if access_mode == 'WO': access_mode = 'RW'
                    complete_line = "        " + \
                        f'{{"label": "{offset_line[0]}",' + \
                        f' "type": "{access_mode}",' + \
                        f' "address": "{offset_line[1]}",' + \
                        f' "desc": "{offset_line[0]}"' + \
                        '},'
                    text_out(complete_line, fout)
                    get_access_mode = True

    text_out('    ]', fout)
    text_out('}', fout)
    fout.close()



def main():
    parser = argparse.ArgumentParser(
        description="Script that translate the vendor header file to json regs file"
    )
    parser.add_argument("-i", "--infile", type=str,
                        required=True, help="vendor header file")
    parser.add_argument("-o", "--output", type=str,
                        required=True, help="output json regs file")
    parser.add_argument("-l", "--space-label", type=str,
                        required=True, help="define space label")
    parser.add_argument("-a", "--address-offset", type=str,
                        required=True, help="define address offset")
    parser.add_argument("-n", "--space-full-name", type=str,
                        required=True, help="define space full name")
    args = parser.parse_args()
    translate_vendor_header_file(**vars(args))


if __name__ == "__main__":
    if sys.version_info < (3, 6, 0):
        sys.stderr.write("You need python 3.6 or later to run this script\n")
        sys.exit(1)

    main()
