"""Python script to create json file from r5560 text registers file.
"""

import argparse
from typing import List, TextIO


def text_out(text: str, fout: TextIO):
    fout.write(text + '\n')


def convert_r5560_regs_file(
    infile: str,
    output: str,
    space_label: str,
    address_offset: str,
    space_full_name: str,
    ring_number: List[int],
    node_number: int
) -> None:
    """Translate R5560 regs txt file to registers json file."""

    fout = open(output + ".json", "w")

    print(f"Input text addrmap file: {infile}\nSpace label: {space_label}\n" \
          f"Space Full Name: {space_full_name}\nAddress Offset: {address_offset}\n" \
          f"Rings: {ring_number}\nNumber of Nodes: {node_number}")

    text_out("{", fout)
    text_out('    "project name"     : "Detector Group Readout Master",', fout)
    text_out(f'    "space full name"  : "{space_full_name}",', fout)
    text_out(f'    "space label"      : "{space_label}",', fout)
    text_out('    "axi_library"      : "dgro_fea",', fout)
    text_out('    "addr bus width"   : "32",', fout)
    text_out(f'    "address offset"   : "{address_offset}",', fout)
    text_out('    "parameter map"    : [', fout)

    with open(infile) as f:
        lines = f.readlines()
        complete_line = ""
        for line in lines:
            access_mode = "RW"
            line_args = line.split()
            register = line_args[0]
            if "SCI_REG_TUBE" in register:
                if not "CONFIG" in register:
                    access_mode = 'RO'

            node = int(register.rsplit('_', 1)[-1])
            ring = int(register.rsplit('_', 2)[-2])

            register = f"{register.rsplit('_', 2)[0]}_R{ring:02d}_N{node:02d}"

            if ring_number is not None:
                if ring not in ring_number:
                    continue
                
            if node >= node_number:
                continue

            complete_line = "        " + \
                f'{{"label": "{register}",' + \
                f' "type": "{access_mode}",' + \
                f' "address": "{line_args[1]}",' + \
                f' "desc": "{register}"' + \
                '},'
            text_out(complete_line, fout)

    text_out('    ]', fout)
    text_out('}', fout)
    fout.close()
    print(f"File {fout.name} created")


def main():
    parser = argparse.ArgumentParser(
        description="Script that translate the r5560 registers text file into json regs file"
    )
    parser.add_argument("-i", "--infile", type=str,
                        required=True, help="address file")
    parser.add_argument("-o", "--output", type=str,
                        required=True, help="output json regs file")
    parser.add_argument("-l", "--space-label", type=str,
                        required=True, help="define space label")
    parser.add_argument("-a", "--address-offset", type=str,
                        required=True, help="define address offset")
    parser.add_argument("-sn", "--space-full-name", type=str,
                        required=True, help="define space full name")
    parser.add_argument("-r", "--ring-number", type=int, nargs='+',
                        help="define number of rings")
    parser.add_argument("-n", "--node-number", type=int,
                        help="define number of nodes", default=31)                    
    args = parser.parse_args()
    convert_r5560_regs_file(**vars(args))


if __name__ == "__main__":
    main()
